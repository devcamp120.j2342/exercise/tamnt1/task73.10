package com.devcamp.pizzaapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.pizzaapi.models.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
    Order findByUser_Id(Long userId);

    @Query("SELECT o, u FROM Order o JOIN o.user u")
    List<Order> findAllWithUser();
}
