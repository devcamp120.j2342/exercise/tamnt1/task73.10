package com.devcamp.pizzaapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizzaapi.models.Menu;

public interface MenuRepository extends JpaRepository<Menu, Integer> {

}
