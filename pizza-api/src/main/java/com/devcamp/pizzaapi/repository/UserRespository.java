package com.devcamp.pizzaapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.devcamp.pizzaapi.models.User;

public interface UserRespository extends CrudRepository<User, Long> {

}
