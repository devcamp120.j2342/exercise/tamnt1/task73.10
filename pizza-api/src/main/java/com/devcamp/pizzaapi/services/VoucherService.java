package com.devcamp.pizzaapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.devcamp.pizzaapi.models.Voucher;
import com.devcamp.pizzaapi.repository.VoucherRepository;

@Service
public class VoucherService {

        private final VoucherRepository voucherRepository;

        public VoucherService(VoucherRepository voucherRepository) {
                this.voucherRepository = voucherRepository;
        }

        public List<Voucher> getVouchers() {
                return voucherRepository.findAll();
        }

        public Voucher getVoucherById(Integer id) {
                Optional<Voucher> optionalVoucher = voucherRepository.findById(id);
                return optionalVoucher.orElse(null);
        }

        public Voucher createVoucher(Voucher voucher) {
                return voucherRepository.save(voucher);
        }

        public Voucher updateVoucher(Integer id, Voucher updatedVoucher) {
                Voucher voucher = getVoucherById(id);
                if (voucher != null) {
                        voucher.setMaVoucher(updatedVoucher.getMaVoucher());
                        voucher.setPhamTramGiamGia(updatedVoucher.getPhamTramGiamGia());
                        voucher.setGhiChu(updatedVoucher.getGhiChu());
                        return voucherRepository.save(voucher);
                }
                return null;
        }

        public void deleteVoucher(Integer id) {
                voucherRepository.deleteById(id);
        }
}
