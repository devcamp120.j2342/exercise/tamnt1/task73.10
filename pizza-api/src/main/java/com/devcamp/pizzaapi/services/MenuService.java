package com.devcamp.pizzaapi.services;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import com.devcamp.pizzaapi.models.Menu;
import com.devcamp.pizzaapi.repository.MenuRepository;

@Service
public class MenuService {

    private final MenuRepository menuRepository;

    public MenuService(MenuRepository menuRepository) {
        this.menuRepository = menuRepository;
    }

    public List<Menu> getMenuList() {
        return menuRepository.findAll();
    }

    public Menu getMenuById(Integer id) {
        Optional<Menu> optionalMenu = menuRepository.findById(id);
        return optionalMenu.orElse(null);
    }

    public Menu createMenu(Menu menu) {
        return menuRepository.save(menu);
    }

    public Menu updateMenu(Integer id, Menu updatedMenu) {
        Optional<Menu> optionalMenu = menuRepository.findById(id);
        if (optionalMenu.isPresent()) {
            Menu existingMenu = optionalMenu.get();
            existingMenu.setSize(updatedMenu.getSize());
            existingMenu.setDuongKinh(updatedMenu.getDuongKinh());
            existingMenu.setSuon(updatedMenu.getSuon());
            existingMenu.setSalad(updatedMenu.getSalad());
            existingMenu.setNuoc(updatedMenu.getNuoc());
            existingMenu.setThanhTien(updatedMenu.getThanhTien());
            return menuRepository.save(existingMenu);
        } else {
            return null;
        }
    }

    public void deleteMenu(Integer id) {
        menuRepository.deleteById(id);
    }
}
