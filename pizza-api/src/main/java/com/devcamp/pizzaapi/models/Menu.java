package com.devcamp.pizzaapi.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "menu")
@EntityListeners(AuditingEntityListener.class)
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 30)
    private String size;
    @Column(length = 30)
    private String duongKinh;
    @Column(length = 30)
    private int suon;
    @Column(length = 30)
    private int salad;
    @Column(length = 30)
    private int nuoc;
    @Column(length = 30)
    private String thanhTien;

    public Menu() {
    }

    public Menu(String size, String duongKinh, int suon, int salad, int nuoc, String thanhTien) {
        this.size = size;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.nuoc = nuoc;
        this.thanhTien = thanhTien;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuon() {
        return suon;
    }

    public void setSuon(int suon) {
        this.suon = suon;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public int getNuoc() {
        return nuoc;
    }

    public void setNuoc(int nuoc) {
        this.nuoc = nuoc;
    }

    public String getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(String thanhTien) {
        this.thanhTien = thanhTien;
    }

    @Override
    public String toString() {
        return "Menu [size=" + size + ", duongKinh=" + duongKinh + ", suon=" + suon + ", salad=" + salad
                + ", thanhTien=" + thanhTien + "]";
    }

}
